import { config } from "dotenv"
config()

import { RefreshingAuthProvider } from "@twurple/auth"
import { ChatClient } from "@twurple/chat"
import { VipBot } from "./VipBot"
import { readFile, writeFile } from "fs/promises"
import createDebug from "debug"

const debug = createDebug('twitch:main')

const TOKENS_EXAMPLE_FILE = './tokens.example.json'

const clientId = (process.env.TWITCH_CLIENT_ID || '').trim()
const clientSecret = (process.env.TWITCH_CLIENT_SECRET || '').trim()
const channels = ((process.env.TWITCH_CHANNELS || '').trim()).split(/[\s,;]+/)
const tokensFile = (process.env.TWITCH_TOKEN_FILE || './tokens.json').trim()

const maxVipsPerChannel = Number.parseInt((process.env.TWITCH_MAX_VIPS_PER_CHANNEL || '100').trim())
const cleanupInterval = Number.parseInt((process.env.TWITCH_VIPS_CLEANUP_INTERVAL || '60000').trim())
const maxInactivityInterval = Number.parseInt((process.env.TWITCH_MAX_INACTIVITY_INTERVAL || '3600000').trim())

/**
 * main startup function
 */
async function main() {

    // function that stores the token JSON file containing the authentication codes
    const storeToken = async (newToken: any) => await writeFile(tokensFile, JSON.stringify(newToken, null, 2), 'utf-8')

    // function that loads the token JSON file containing the authentication codes
    const loadToken = async () => {
        let tokensFileContents: any

        try {
            tokensFileContents = JSON.parse(await readFile(tokensFile, 'utf-8'))
        } catch {
            tokensFileContents = JSON.parse(await readFile(TOKENS_EXAMPLE_FILE, 'utf-8'))
        }
        
        return tokensFileContents
    }

    // load initial token from file
    const initialToken = await loadToken()
    debug('initial token loaded')

    // create auth provider that can update it's access token
    const authProvider = new RefreshingAuthProvider({
        clientId,
        clientSecret,
        onRefresh: async (newToken: any) => {
            await storeToken(newToken)
            debug('token refreshed')
        },
    }, {
        accessToken: initialToken.access_token || initialToken.accessToken,
        expiresIn: initialToken.expires_in || initialToken.expiresIn,
        refreshToken: initialToken.refresh_token || initialToken.refreshToken,
        scope: initialToken.scope,
        obtainmentTimestamp: initialToken.obtainmentTimestamp || 0
    })

    // the Twitch chat client
    const chatClient = new ChatClient({
        logger: { colors: true, emoji: true },
        authProvider,
        channels,
        // isAlwaysMod: true
    })

    // connect client
    await chatClient.connect()
    debug('client connected')

    // initialize AutoVipBot
    const autoVipBot = new VipBot({
        chatClient,
        maxVipsPerChannel,
        cleanupInterval,
        maxInactivityInterval
    })

    // start enabled
    // autoVipBot.enabled

    debug(`bot enabled: ${autoVipBot.enabled}`)
    debug('enter "!autovip 1" or "!autovip 0" in one of the channels')

}

main().catch(err => debug('ERROR: %s', err.stack || err.message || err))
