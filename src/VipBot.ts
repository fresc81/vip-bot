
import { Listener } from "@d-fischer/typed-event-emitter"
import { ChatClient } from "@twurple/chat"
import createDebug from "debug"

const debug = createDebug('twitch:vip-bot')

/**
 * holds the last activity data for a particular user
 */
interface LastActiveItem {
    username: string
    channel: string
    timestamp: number
}

/**
 * bot options for the VipBot constructor
 */
export interface VipBotOptions {
    maxVipsPerChannel?: number
    maxInactivityInterval?: number
    cleanupInterval?: number
    chatClient: ChatClient
}

/**
 * a bot that automatically VIPs all users who are active in a twitch channel
 */
export class VipBot {

    private _enabled = false
    private _timer: NodeJS.Timeout | null = null
    private _lastActiveItems: LastActiveItem[] = []
    private _chatListener: Listener | null = null
    private _joinListener: Listener | null = null

    private readonly _maxVipsPerChannel: number = 0
    private readonly _maxInactivityInterval: number = 0
    private readonly _cleanupInterval: number = 0
    private readonly _chatClient: ChatClient

    /**
     * create a new VipBot instance
     * @param options pass an VipBotOptions object, the chatClient is mandatory
     */
    public constructor(options: VipBotOptions) {

        const cleanupInterval = options.cleanupInterval || 60000
        this._cleanupInterval = cleanupInterval

        const maxInactivityInterval = options.maxInactivityInterval || 3600000
        this._maxInactivityInterval = maxInactivityInterval

        const maxVipsPerChannel = options.maxVipsPerChannel || 100
        this._maxVipsPerChannel = maxVipsPerChannel

        const chatClient = options.chatClient
        this._chatClient = chatClient

        this._chatListener = chatClient.onMessage(async (channel, username, text, msg) => {
            const isBroadcasterOrMod = msg.userInfo.isBroadcaster || msg.userInfo.isMod
            this.onIncomingMessage(channel, username, text, isBroadcasterOrMod)
        })

        this._joinListener = chatClient.onJoin(async (channel, username) => {
            debug('joined : %s @ %s', username, channel)
        })

    }

    /**
     * stop the VipBot and release all resources
     */
    public close() {
        this.enabled = false

        this._chatListener?.unbind()
        this._chatListener = null

        this._joinListener?.unbind()
        this._joinListener = null
    }

    /**
     * gets or sets a value specifying if the bot is active or not
     */
    public set enabled(value: boolean) {
        if (this._enabled !== value) {
            this._enabled = value
            switch (value) {
                case true:
                    this.onEnabled()
                    break
                case false:
                    this.onDisabled()
                    break
            }
        }
    }
    public get enabled() {
        return this._enabled
    }

    /**
     * gets all VIPs that are locally stored for a particular channel
     * @param channel the channel to look up the VIPs for
     * @returns a list of LastActiveItem objects for the given channel
     */
    private getVipsForChannel(channel: string) {
        return this._lastActiveItems
            .filter(lastActiveItem => lastActiveItem.channel === channel)
            .sort((a, b) => a.timestamp - b.timestamp)
    }

    /**
     * VIPs a user
     * if there are no more VIP slots left the user that has been inactive
     * the longest is implicitly removed from the VIPs
     * @param username the user to VIP
     * @param channel the channel on which the user should be VIPd
     */
    private vipUser(username: string, channel: string) {
        const vipsForChannel = this.getVipsForChannel(channel)

        while (vipsForChannel.length >= this._maxVipsPerChannel) {
            const oldestVipUser = vipsForChannel.shift()
            if (oldestVipUser) {
                this.unvipUser(oldestVipUser.username, oldestVipUser.channel)
            }
        }

        debug('vip user %s @ %s', username, channel)
        this._chatClient
            .addVip(channel, username)
            .catch(err => this.onError(err))
    }

    /**
     * UnVIPs a user
     * @param username the user to UnVIP
     * @param channel the channel on which the user should be UnVIPd
     */
    private unvipUser(username: string, channel: string) {
        debug('unvip user %s @ %s', username, channel)
        this._chatClient
            .removeVip(channel, username)
            .catch(err => this.onError(err))
    }

    /**
     * called when the bot is activated
     */
    private readonly onEnabled = () => {
        debug('enabled')
        this._timer = setInterval(this.onTimer, this._cleanupInterval)
    }

    /**
     * called when the bot is deactivated
     */
    private readonly onDisabled = () => {
        if (this._timer) {
            debug('disabled')
            clearInterval(this._timer)
            this._timer = null
        }
    }

    /**
     * called when the internal cleanup timer fires.
     * removes inactive users and UnVIPs them
     */
    private readonly onTimer = () => {
        debug('timer')
        this._lastActiveItems = this._lastActiveItems
            .filter(lastActive => {
                const diffTime = Date.now() - lastActive.timestamp
                const isInactive = diffTime > this._maxInactivityInterval
                if (isInactive) {
                    this.unvipUser(lastActive.username, lastActive.channel)
                }
                return !isInactive
            })
    }

    /**
     * handles incoming twitch messages
     * @param channel the channel name
     * @param username the name of the author
     * @param text the message contents
     * @param isBroadcasterOrMod true if the message was written by a mod or a broadcaster
     */
    private readonly onIncomingMessage = (channel: string, username: string, text: string, isBroadcasterOrMod: boolean) => {
        debug('message %s @ %s: %s' + (isBroadcasterOrMod ? ' [MOD]' : ''), username, channel, text)

        if (this._enabled) {
            this.updateUser(username, channel)
        }

        if (isBroadcasterOrMod) {
            const command = text.trim().split(/\s+/)
            switch (command[0].toLowerCase()) {

                case '!autovip':
                    this.onAutoVipCommand(channel, command[1])
                    break

                case '!vip':
                    if (this.getVipsForChannel(channel).filter(vip => vip.username === username).length == 0) {
                        this.vipUser(command[1], channel)
                    }
                    break

                case '!unvip':
                    if (this.getVipsForChannel(channel).filter(vip => vip.username === username).length > 0) {
                        this.unvipUser(command[1], channel)
                    }
                    break

            }
        }
    }

    /**
     * handles the !autovip <on|off> command
     * @param param a value indicating the new enabled state
     */
    private readonly onAutoVipCommand = (channel: string, param: string) => {
        debug('command !autovip %s', param)
        switch (param.toLowerCase()) {

            case '1':
            case 'true':
            case 'on':
            case 'yes':
                if (!this.enabled) {
                    this.enabled = true
                    this._chatClient
                        .say(channel, '✅ AutoVIP enabled ✅')
                        .catch(err => this.onError(err))
                }
                break

            case '0':
            case 'false':
            case 'off':
            case 'no':
                if (this.enabled) {
                    this.enabled = false
                    this._chatClient
                        .say(channel, '⛔ AutoVIP disabled ⛔')
                        .catch(err => this.onError(err))
                }
                break
        }
    }

    /**
     * Log Twitch API error
     * @param err error object
     */
    private readonly onError = (err: any) => {
        debug('ERROR: %s', err.stack || err.message || err)
    }

    /**
     * updates a users activity timestamp
     * @param username the active user
     * @param channel the channel on which the user was active
     */
    private updateUser(username: string, channel: string) {
        let lastActiveItem = this._lastActiveItems.find(lastActiveItem => (lastActiveItem.username === username) && (lastActiveItem.channel === channel))
        if (!lastActiveItem) {
            debug('start activity for %s @ %s', username, channel)
            lastActiveItem = { username, channel, timestamp: Date.now() }
            this._lastActiveItems.push(lastActiveItem)
            this.vipUser(username, channel)
        } else {
            debug('update activity for %s @ %s', username, channel)
            lastActiveItem.timestamp = Date.now()
        }
    }

}
